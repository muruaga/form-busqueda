<?php

/**
 * Class DataModelos
 * Consulta modelos a BBDD.
 */

class DataModelos {

	private $mysqli;
	private $params;


	/**
	 * DataModelos constructor.
	 *
	 * @param object $mysqli Conexión a BBDD.
	 * @param string $tipovehiculo Indentificador tipo vehículo.
	 */
	public function __construct( $mysqli, $params ) {
		$this->mysqli = $mysqli;
		$this->params = $params;
	}


	/**
	 * Devuelve la lista de Modelos según marca.
	 *
	 * @param string $marca Identificador de marca.
	 *
	 * @return array
	 * @throws Exception
	 */
	public function getBy( $marca ) {

		$sql = " SELECT mlocode AS value" .
		       " FROM " . DB_PREFIJO . "vehiculos" .
		       " INNER JOIN " . DB_PREFIJO . "vehiculoDatosEconomicos" .
		       " ON " . DB_PREFIJO . "vehiculos.id_vehiculos = " . DB_PREFIJO . "vehiculoDatosEconomicos.id_vehiculo";

		switch ( $this->params['t'] ) {
			case TIPO_VEHICULO_OCASION:
				$sql .= " WHERE vn_vo = '1' AND km0 = 0";
				if ( PVP_OFERTA_PARITCULARES ) {
					$sql .= " AND pvpOfertaParticulares = 0";
				}
				break;

			case TIPO_VEHICULO_OCASION_MAS_KM0:
				$sql .= " WHERE vn_vo = '1'";
				if ( PVP_OFERTA_PARITCULARES ) {
					$sql .= " AND pvpOfertaParticulares = 0";
				}
				break;

			case TIPO_VEHICULO_OCASION_NO_FURGON:
				$sql .= " WHERE vn_vo = '1' AND km0 <> 2 AND automatico=1";
				if ( PVP_OFERTA_PARITCULARES ) {
					$sql .= " AND pvpOfertaParticulares = 0";
				}
				break;

			case TIPO_VEHICULO_OCASION_FURGON:
				$sql .= " WHERE automatico IS NULL AND km0 <> 2";
				break;

			case TIPO_VEHICULO_NUEVO:
				$sql .= " WHERE vn_vo = '0'";
				if ( PVP_OFERTA_PARITCULARES ) {
					$sql .= " AND pvpOfertaParticulares = 0";
				}
				break;

			case TIPO_VEHICULO_OCASION_OFERTA:
				$sql .= " WHERE vn_vo = '1'";
				if ( PVP_OFERTA_PARITCULARES ) {
					$sql .= " AND pvpOfertaParticulares > 0";
				}
				break;

			case TIPO_VEHICULO_NUEVO_OFERTA:
				$sql .= " WHERE vn_vo = '0'";
				if ( PVP_OFERTA_PARITCULARES ) {
					$sql .= " AND pvpOfertaParticulares > 0";
				}
				break;

            case TIPO_VEHICULO_DEMO:
                $sql .= " WHERE km0 >= 1";
                if (PVP_OFERTA_PARITCULARES) {
                  $sql .= " AND pvpOfertaParticulares = 0";
                }
            break;

            case TIPO_VEHICULO_SUBASTA:
                $sql .= " WHERE vn_vo = '1' and subasta = 1";
            break;

            case TIPO_VEHICULO_OCASION_NO_SUBASTA:
                $sql .= " WHERE vn_vo = '1' and subasta IS NULL";
            break;

            case TIPO_VEHICULO_KM0_NO_SUBASTA:
                $sql .= " WHERE vn_vo = '1' and km0 = 1";
            break;

            case TIPO_VEHICULO_ECO:
                $sql .= " WHERE etiquetado = 'eco' or etiquetado = '0'";
            break;

            case TIPO_VEHICULO_KM0:
                $sql .= " WHERE km0 = 1";
            break;

            default:
				$sql .= " WHERE km0 >= 0";
			break;
		}

		$sql .= " AND maknatcode = '" . $marca . "'";
		$sql .= " GROUP BY mlocode";

		$result = $this->mysqli->query( $sql );
		if ( ! $result ) {
			throw new Exception( $this->mysqli->error );
		}

		return $result;
	}

}