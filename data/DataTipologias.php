<?php


/**
 * Class DataTipologias
 * Consulta tipologias en BBDD.
 */


class DataTipologias {


	/**
	 * Devuelve las tipologias
	 *
	 * @param object $mysqli Conexión a BBDD.
	 * @param string $tipovehiculo Indentificador tipo vehículo.
	 *
	 * @return mixed
	 * @throws Exception
	 */
	public function getBy( $mysqli, $params ) {
		$sql = " SELECT StructureDescription AS value" .
		       " FROM " . DB_PREFIJO . "vehiculos";

		switch ( $params ) {

			case TIPO_VEHICULO_NUEVO:
				$sql .= " WHERE vn_vo = '0'";
				if ( PVP_OFERTA_PARITCULARES ) {
					$sql .= " AND pvpOfertaParticulares = 0";
				}
				break;

			case TIPO_VEHICULO_OCASION:
				$sql .= " WHERE vn_vo = '1' AND km0 = 0";
				if ( PVP_OFERTA_PARITCULARES ) {
					$sql .= " AND pvpOfertaParticulares = 0";
				}
				break;

			case TIPO_VEHICULO_OCASION_MAS_KM0:
				$sql .= " WHERE vn_vo = '1'";
				if ( PVP_OFERTA_PARITCULARES ) {
					$sql .= " AND pvpOfertaParticulares = 0";
				}
				break;

			case TIPO_VEHICULO_OCASION_NO_FURGON:
				$sql .= " WHERE vn_vo = '1' AND km0 <> 2 AND automatico=1";
				if ( PVP_OFERTA_PARITCULARES ) {
					$sql .= " AND pvpOfertaParticulares = 0";
				}
				break;

			case TIPO_VEHICULO_OCASION_FURGON:
				$sql .= " WHERE automatico IS NULL AND km0 <> 2";
				break;

			case TIPO_VEHICULO_OCASION_OFERTA:
				$sql .= " WHERE vn_vo = '1'";
				if ( PVP_OFERTA_PARITCULARES ) {
					$sql .= " AND pvpOfertaParticulares > 0";
				}
				break;

			case TIPO_VEHICULO_NUEVO_OFERTA:
				$sql .= " WHERE vn_vo = '0'";
				if ( PVP_OFERTA_PARITCULARES ) {
					$sql .= " AND pvpOfertaParticulares > 0";
				}
				break;

            case TIPO_VEHICULO_DEMO:
                $sql .= " WHERE km0 >= 1";
                if (PVP_OFERTA_PARITCULARES) {
                  $sql .= " AND pvpOfertaParticulares = 0";
                }
            break;

            case TIPO_VEHICULO_SUBASTA:
                $sql .= " WHERE vn_vo = '1' and subasta = 1";
            break;

            case TIPO_VEHICULO_OCASION_NO_SUBASTA:
                $sql .= " WHERE vn_vo = '1' and subasta IS NULL";
            break;

            case TIPO_VEHICULO_KM0_NO_SUBASTA:
                $sql .= " WHERE vn_vo = '1' and km0 = 1";
            break;

            case TIPO_VEHICULO_ECO:
                $sql .= " WHERE etiquetado = 'eco' or etiquetado = '0'";
            break;

            case TIPO_VEHICULO_KM0:
                $sql .= " WHERE km0 = 1";
            break;

            default:
                  $sql .= " WHERE km0 >= 0";
                  break;
		}

		$sql .= " GROUP BY StructureDescription";

		$result = $mysqli->query( $sql );
		if ( ! $result ) {
			throw new Exception( $mysqli->error );
		}

		return $result;
	}
}