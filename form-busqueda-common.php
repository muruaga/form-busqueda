<?php



/**
 * Devuelve conexión a BBDD.
 * @return {object} mysqli - Instancia.
 */
function conectionDB() {
	$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
	$mysqli->set_charset('utf8');
	if ($mysqli->connect_error) { // Die Error de connexión.
		die("Connection failed: " . $mysqli->connect_error);
	}
	return $mysqli;
}



/**
 * Cierra conexión BBDD.
 * @param {object} $mysqli - Instancia.
 */
function closeDB($mysqli) {
	$mysqli->close();
}



/**
 * Devuelve la platilla HTML con valores.
 * @param string $str Plantilla HTML.
 * @param array $arr_values Valores a incluir.
 *
 * @return string
 */
function strReplace($str, $arr_values) {
	$return =  preg_replace_callback('~\{\$(.*?)\}~si',
		function($match) use ($arr_values) {
			return str_replace($match[0], isset($arr_values[$match[1]]) ? str_replace(' ', '_',$arr_values[$match[1]]) : $match[0], $match[0]);
		},
		$str);
	return $return;
}


function floatCV ($value) {
	$cv = (float) $value * ( 1.3596 );
	$cv = round( $cv );
	return $cv;
}


/*function strReplacePath($str, $arr_values) {
	$return = strReplace($str, $arr_values);
	$return = str_replace( ' ', '_', $return );
	return $return;
}*/



function numberFormat ($number) {
	$format = intval($number);
	$format = number_format( $format, 0, "", "." );
	return $format;
}

function replaceEspacio($arr_values) {
    $return = str_replace( ' ', '', $arr_values  );
    return $return;
}
function replaceGuion($arr_values){
    $espacio = replaceEspacio($arr_values);
    $return = str_replace( '-', '', $espacio );
    return $return;
}

/**
 * Reemplaza los espacios por guiones y quita todas las mayusculas y acentos
 */
function replaceAcentoGuion($string){
  $originales = 'àáâãäçèéêëìíîïñòóôõöùúûüýÿ';
  $modificadas = 'aaaaaceeeeiiiinooooouuuuyy';
  $id = strtr(utf8_decode($string), utf8_decode($originales), $modificadas);
  $id = strtolower($id);
  $idfinal = preg_replace('/(?![\xC0-\xff])(\W)/', '_', $id);
  return $idfinal;
}

function splitMatricula($arr_values){
    $parts = replaceGuion($arr_values);
    //separa los numeros de las letras
    $parts = preg_split("/(,?\s+)|((?<=[a-z])(?=\d))|((?<=\d)(?=[a-z]))/i", $parts);
    return $parts[0].'%20'.$parts[1];
}

function getTarifa($arr_values){
    if($arr_values == FINANCIACION_COD_PRODUCTO_VO_MAYOR_5_ANNOS){
      $return = FINANCIACION_COD_TARIFA_USADO;
    }else{
      $return = FINANCIACION_COD_TARIFA_NUEVO_SEMINUEVO;
    }
    return $return;
}

function getModelo( $att_1, $att_2){
  ($att_1 == 'DS') ? $modelo = 'DS-'.$att_2 : $modelo = $att_2;
  return $modelo;
}

function tipoURL($value, $att_1, $att_2){
    switch ($value){
        case 0:
            return '';
        break;
        case 1:
            return '&g-v-p='.$att_1.'&g-v-y='.$att_2;
        break;
    }
}

function tiempoRestante($fecha){
  $rem = strtotime($fecha) - time();
  $dia = floor($rem / 86400);
  $hr = floor(($rem % 86400) / 3600);
  $min = floor(($rem % 3600) / 60);

  if ($dia <= 0 && $hr <= 0){
    $resultado = '';
  }else{
    if ($dia >= 1 ){
      $resultado = $dia .' días';
    }else{
      $resultado = $hr .'h:' . $min . 'min';
    }}

  return $resultado;
}


/**
 * Devuelve la platilla HTML con valores.
 * @param string $template Plantilla HTML.
 * @param array $arr_values Valores a incluir.
 */
function printTemplate($template, $arr_values) {
	$html =  preg_replace_callback('~\{\$(.*?)\}~si',
		function($match) use ($arr_values) {
			return str_replace($match[0], isset($arr_values[$match[1]]) ? $arr_values[$match[1]] : $match[0], $match[0]);
		},
		$template);

	printf($html);
}

// ERROR Logs

$GLOBALS['time_start'] = null;

function startTimerErrorLog() {
	if ($GLOBALS['time_start'] != null) {
		throw new Exception('TimerErrorLog Exception: Para iniciar se requiere hacer primero stop');
	}
	else {
		$GLOBALS['time_start'] = new DateTime;
	}
}


function stopTimerErrorLog($msg) {
	$time_start = $GLOBALS['time_start'];
	$time_end = new DateTime;
	$time = $time_start->diff($time_end);
	printf(hashStringReplace($msg, array(
		'time'=>$time->format('%s')
	)));
	$GLOBALS['time_start'] = null;
}



function hashStringReplace($str, $hash) {
	$result =  preg_replace_callback('~\{\$(.*?)\}~si',
		function($match) use ($hash) {
			return str_replace($match[0], isset($hash[$match[1]]) ? $hash[$match[1]] : $match[0], $match[0]);
		},
		$str);
	return $result;
}

function parseDataVehiculo($row, $params) {
	$row['precio']  = numberFormat( $row["pvpParticulares"] );
	$row['cv']      = floatCV($row["typhp"]);
	$row['oferta']  = numberFormat( $row["pvpOfertaParticulares"] );
    $row['precioF'] = numberFormat( $row["pvpFinanciado"] );
	$row['km']      = numberFormat( $row["kms"] );
	$row['mat-finan'] = splitMatricula( $row["matricula"] );
	$row['tipoURL'] = tipoURL( $row["vn_vo"], $row['mat-finan'], $row['fechatotal'] );
	$row['marca-finan'] = ($row['maknatcode'] == 'DS') ? 'Citroen' : $row['maknatcode'];
    $row['modelo-finan'] = getModelo(  $row['maknatcode'], $row['mlocode'] );
	$row['tarifa'] = getTarifa($row["codProducto"]);
	$row['finOferta'] = tiempoRestante($row['fcOfertaParticulares']);

	if (!empty($params['path_info'])) {
      $row['ficha'] = strReplace( '/'.$params['path_info'].VEHICULO_INFO_PATH, $row );
	}else if(!empty(VEHICULO_INFO_PATH)){
      $row['ficha'] = strReplace( '/vehiculo-info'.VEHICULO_INFO_PATH, $row );
    }
	if (!empty(VEHICULO_FINANCIACION_PATH)) {
		$row['financiacion']  = strReplace( VEHICULO_FINANCIACION_PATH, $row );
	}
	return $row;
}


/**
 * Devuelve si la petición es tipo POST
 * @return bool
 */
function isPostRequestMethod() {
	return ($_SERVER['REQUEST_METHOD'] === 'POST');
}


/**
 * Devuelve el valor de un campo dependiendo del tipo de petición.
 * @param $name
 *
 * @return mixed
 */
function getValueOnRequest($name) {
	if (isPostRequestMethod()) {
		return $_POST[$name];
	}
	else {
		return $_GET[$name];
	}
}