(function ($) {
  $(document).ready(function () {

    var $containerVehiculos =  $('#ContenedorVehiculos');
    var actualPage = 1;

    var $marcasele = '';
    // Búsqueda por cambio de marca.
    $('select.marca').change(function () {
      var $marca = ($(this).hasClass('mobile')) ? $('select.marca.mobile') : $(this);
      $marcasele = $marca;
      var tipoVehiculo = $marca.attr('t-vehiculo');
      var path_info = $containerVehiculos.find('.listado').attr('path_info');
      var shownoimg = $containerVehiculos.find('.listado').attr('shownoimg');
      cargaModelosCombustibles(tipoVehiculo, $marca.val());
      if (!$marca.is('[no-filter]')) {
        var query = getBusquedaParams(tipoVehiculo, path_info, shownoimg);
        buscarVehiculos(query);
      }
    });


    // Búsqueda por cambio de modelo.
    $('select.modelo').change(function () {
      var $modelo = ($(this).hasClass('mobile')) ? $('select.modelo.mobile') : $(this);
      var tipoVehiculo = $modelo.attr('t-vehiculo');
      var path_info = $containerVehiculos.find('.listado').attr('path_info');
      var shownoimg = $containerVehiculos.find('.listado').attr('shownoimg');
      cargaCombustibles(tipoVehiculo, $marcasele.val(), $modelo.val());
      if (!$modelo.is('[no-filter]')) {
        var query = getBusquedaParams(tipoVehiculo, path_info, shownoimg);
        buscarVehiculos(query);
      }
    });


    // Búsqueda por cambio otro tipo de select.
    $('select.selectores').change(function () {
      var $selectores = ($(this).hasClass('mobile')) ? $('select.selectores.mobile') : $(this);
      var tipoVehiculo = $selectores.attr('t-vehiculo');
      var path_info = $containerVehiculos.find('.listado').attr('path_info');
      var shownoimg = $containerVehiculos.find('.listado').attr('shownoimg');
      if (!$selectores.is('[no-filter]')) {
        var query = getBusquedaParams(tipoVehiculo, path_info, shownoimg);
        buscarVehiculos(query);
      }
    });

    // Búsqueda por cambio de precio, km, anno en mobile.
    $('select.precio, select.km, select.anno').change(function () {
      var $select = ($(this).hasClass('mobile')) ? $('select.precio.mobile, select.km.mobile, select.anno.mobile') : $(this);
      var classMobile = ($(this).hasClass('mobile')) ? '.mobile' : '';
      var val = $select.val();
      var namemin = $select.attr('name').split('min')[1];
      var namemax = $select.attr('name').split('max')[1];
      var tipoVehiculo = $select.attr('t-vehiculo');
      var path_info = $containerVehiculos.find('.listado').attr('path_info');
      var shownoimg = $containerVehiculos.find('.listado').attr('shownoimg');
      if (!$select.is('[no-filter]')) {
        var query = getBusquedaParams(tipoVehiculo, path_info, shownoimg);
        buscarVehiculos(query);
      }
      if (namemin){
        $('select.'+namemin+classMobile+'[name="max'+namemin+'"] option').each(function () {
          var opcion = $(this).val();
          if (!isNaN(opcion) && parseInt(opcion) < parseInt(val)){
            $(this).attr('disabled','disabled');
          }
        });
      }else if (namemax){
        $('select.'+namemax+classMobile+'[name="min'+namemax+'"] option').each(function () {
          var opcion = $(this).val();
          if (!isNaN(opcion) && parseInt(opcion) > parseInt(val)){
            $(this).attr('disabled','disabled');
          }
        });
      }
    });

    // Búsqueda por cambio de order.
    $('select.order').change(function () {
      var $order = ($(this).hasClass('mobile')) ? $('select.order.mobile') : $(this);
      var tipoVehiculo = $order.attr('t-vehiculo');
      var path_info = $containerVehiculos.find('.listado').attr('path_info');
      var shownoimg = $containerVehiculos.find('.listado').attr('shownoimg');
      if (!$order.is('[no-filter]')) {
        var query = getBusquedaParams(tipoVehiculo, path_info, shownoimg);
        buscarVehiculos(query);
      }
      else {
        $('input[name="orderby"][t-vehiculo="' + tipoVehiculo + '"], input[name="orderbydesc"][t-vehiculo="' + tipoVehiculo + '"]').val('');
        var value = $order.val();
        var type = $order.find('option[value="' + value +'"]').attr('type');
        if (type === 'orderbydesc') {
          value = value.split('-desc')[0];
        }
        $('input[name="' + type + '"][t-vehiculo="' + tipoVehiculo + '"]').val(value);
      }
    });

    // Búsqueda por cambio de precio, km, anno.
    $('.slider').each(function () {
      var $slider = $(this);
      var tipoVehiculo = $slider.attr('t-vehiculo');
      var path_info = $containerVehiculos.find('.listado').attr('path_info');
      var shownoimg = $containerVehiculos.find('.listado').attr('shownoimg');
      var name = $slider.attr('name');
      var min = 0;
      if ($slider.attr('min').length) {
        min = parseInt($slider.attr('min'));
      }
      var max = 0;
      if ($slider.attr('max').length) {
        max = parseInt($slider.attr('max'));
      }
      var unit = $slider.attr('unit');

      var startmin = 0;
      var startmax = 0;
      if ($slider.is('[values]')) {
        var values = $slider.attr('values').split('|');
        if (values.length === 2) {
          startmin = parseInt(values[0]);
          startmax = parseInt(values[1]);
        }else{
          startmin = min;
          startmax = max;
        }
      }else{
        startmin = min;
        startmax = max;
      }

      noUiSlider.create($slider[0], {
        start: [startmin, startmax],
        connect: true,
        step: 2000,
        range: {
          'min': min,
          'max': max
        }
      });

      $slider[0].noUiSlider.on('update', function (values) {
        var $slider = $(this.target);
        $slider.parent().find('.slider-value.min').html(puntosNumero(parseInt(values[0])) + ' ' + unit);
        $slider.parent().find('.slider-value.max').html(puntosNumero(parseInt(values[1])) + ' ' + unit);
        $slider.parent().find('input[name="min' + name + '"]').val(parseInt(values[0]));
        $slider.parent().find('input[name="max' + name + '"]').val(parseInt(values[1]));
      });

      $slider[0].noUiSlider.on('change', function (values) {
        var $slider = $(this.target);
        if (!$slider.is('[no-filter]')) {
          var query = getBusquedaParams(tipoVehiculo, path_info, shownoimg);
          buscarVehiculos(query);
        }
      });
    });


    // Click botón reset.
    $(document).on('click', '.reset', function () {
      var tipoVehiculo = $(this).attr('t-vehiculo');
      var path_info = $containerVehiculos.find('.listado').attr('path_info');
      var shownoimg = $containerVehiculos.find('.listado').attr('shownoimg');
      cleanFormBusqueda(tipoVehiculo);
      var query = getBusquedaParams(tipoVehiculo, path_info, shownoimg);
      buscarVehiculos(query);
    });


    // Click botón clean.
    /*
    $('.clean').on('click', function () {
      var tipoVehiculo = $(this).attr('t-vehiculo');
      cleanFormBusqueda(tipoVehiculo);
    });
    */

    // Click en tipología.
    $('li.tipologia').on('click', function () {
      var $tipologia = $(this);
      $tipologia.parent().find('li.tipologia').removeClass('selected');
      $tipologia.addClass('selected');
      var data = $tipologia.attr('data-value');
      $tipologia.parent().find('input[name="tp"]').val(data);
    });


    // Click en paginación.
    $containerVehiculos.on('click', '.page-item:not(.disabled)', function () {
      var $elem = $(this);
      var pag = actualPage;
      if ($elem.hasClass('prev')) {
        pag--;
      }
      else if ($elem.hasClass('next')) {
        pag++;
      }
      else {
        pag = parseInt($elem.text().trim());
      }
      var query = $containerVehiculos.data('query');
      if (typeof query === 'undefined') {
        var tipoVehiculo = $containerVehiculos.find('.listado').attr('t-vehiculo');
        var path_info = $containerVehiculos.find('.listado').attr('path_info');
        var shownoimg = $containerVehiculos.find('.listado').attr('shownoimg');
        query = getBusquedaParams(tipoVehiculo, path_info, shownoimg);
      }
      query.pag = pag;
      buscarVehiculos(query);
    });

    //Script del comparador
    var vehs_compare = localStorage.getItem('vehs_compare');
    vehs_compare = JSON.parse(vehs_compare);
    var arr_vehi = (vehs_compare !== null) ? vehs_compare : [];
    var contador = $('.contador-number');

    /**
     * El caso de que recargues la página auto checkea los vehiculos
     * que se encuentran aun en el comparador
     */
    var autoCheckCompare = function () {
      $.each(arr_vehi, function (indice, elemento) {
        var label = $('.switch-label-compare[data-veh="' + elemento + '"]');
        label.parent().find('.switch-input-compare').attr('checked', true);
      });
    };
    autoCheckCompare();

    var contadorCheck = function (elems){
      var checked = elems.length;
      if (checked >= 3) {
        $('.switch-input-compare').not(':checked').prop('disabled', true);
      } else {
        $('.switch-input-compare').prop('disabled', false);
      }
      var number = (arr_vehi.length === 0) ? '' : arr_vehi.length;
      contador.text(number);
    };
    contadorCheck(arr_vehi);

    $( document ).ajaxComplete(function() {
      autoCheckCompare();
      contadorCheck(arr_vehi);
    });
    // Click botón comparar.
    $containerVehiculos.on('click', '.switch-input-compare', function () {
      var label = $(this).parent().find('.switch-label-compare');
      var id_vehiculo = label.attr('data-veh');

      if(this.checked){
        arr_vehi.push(id_vehiculo);
        //Animación
        var cart = $('.menu-item-1728 .menu-text');
        var imgtodrag = $(this).parents('.card.box-car').find("img").eq(0);
        if (imgtodrag) {
          var imgclone = imgtodrag.clone()
            .offset({
              top: imgtodrag.offset().top,
              left: imgtodrag.offset().left
            })
            .css({
              'opacity': '0.5',
              'position': 'absolute',
              'width': '150px',
              'z-index': '100'
            })
            .appendTo($('body'))
            .animate({
              'top': cart.offset().top + 10,
              'left': cart.offset().left + 10,
              'width': 75
            }, 1000, 'easeInOutExpo');

          imgclone.animate({
            'width': 0,
            'height': 0
          }, function () {
            $(this).detach()
          });
        }
      }else{
        eliminarVehiculo(id_vehiculo);
      }
      localStorage.setItem('vehs_compare', JSON.stringify(arr_vehi));
      // comprueba si se ha chequeado el boton 3 veces para deshabilitar el resto
      contadorCheck(arr_vehi);
    });
    var irPaginaComparador = function (e) {
      var vehs_compare = localStorage.getItem('vehs_compare');
      vehs_compare = JSON.parse(vehs_compare);
      e.preventDefault();
      /*$.post('/comparador/', { vehs: vehs_compare},
        function(data) {
          var w = window.open('./comparador', '_self');
          w.document.open();
          w.document.write(data);
          w.document.close();
        });*/
      $.redirect('/comparador/', {vehs: vehs_compare});
    };
    $('.compare-close img').on('click', function (e) {
      var id_vehiculo= $(this).parent().attr('data-id');
      eliminarVehiculo(id_vehiculo);
      localStorage.setItem('vehs_compare', JSON.stringify(arr_vehi));
      irPaginaComparador(e);
    });
    $('.menu-item-1728 a').on('click', function (e) {
      irPaginaComparador(e);
    });
    var eliminarVehiculo = function (id) {
      arr_vehi = $.grep(arr_vehi, function (elem, ind) {
        return elem.indexOf(id) === -1;
      })
    };


    var cleanFormBusqueda = function (tipoVehiculo) {
      $('select[t-vehiculo="' + tipoVehiculo + '"]').prop('selectedIndex',0);
      resetModelosCombustibles(tipoVehiculo);
      $('.slider[t-vehiculo="' + tipoVehiculo + '"]').each(function () {
        var $slider = $(this);
        var min = 0;
        if ($slider.attr('min').length) {
          min = parseInt($slider.attr('min'));
        }
        var max = 0;
        if ($slider.attr('max').length) {
          max = parseInt($slider.attr('max'));
        }
        $slider[0].noUiSlider.set([min, max]);
      });
      $('.radio.tipologia[t-vehiculo="' + tipoVehiculo + '"]').removeClass('selected');
      $('select.precio[t-vehiculo="' + tipoVehiculo + '"] option, ' +
        'select.km[t-vehiculo="' + tipoVehiculo + '"] option, ' +
        'select.anno[t-vehiculo="' + tipoVehiculo + '"] option').each(function () {
        var opcion = $(this).val();
        if (!isNaN(opcion)){
          $(this).removeAttr("disabled")
        }
      });
    };



    var resetModelosCombustibles = function (tipoVehiculo) {

      var parametros = {
        action: 'options_ajax',
        t: tipoVehiculo
      };

      $.ajax({
        url: form_busqueda_ajax.url,
        dataType: "json",
        type: 'get',
        data: parametros,
        success: function (response) {
          $('select.modelo[t-vehiculo="' + tipoVehiculo + '"]').html(response.modelos);
          $('select.combustible[t-vehiculo="' + tipoVehiculo + '"]').html(response.combustibles);
        }
      });
    };



    /**
     * Actualiza la lista de modelos y combustible por de la marca.
     * @param {string} tipoVehiculo - Identificador tipo de vehículo.
     * @param {string} marca - Identificador tipo de marca.
     */
    var cargaModelosCombustibles = function (tipoVehiculo, marca) {

      var parametros = {
        action: 'options_ajax',
        t: tipoVehiculo,
        ma: marca
      };

      $('body').addClass('loading');
      $.ajax({
        url: form_busqueda_ajax.url,
        dataType: "json",
        type: 'get',
        data: parametros,
        success: function (response) {
          $('body').removeClass('loading');
          $('select.modelo[t-vehiculo="' + tipoVehiculo + '"]').html(response.modelos);
          var $selectComb = $('select.combustible[t-vehiculo="' + tipoVehiculo + '"]');
          // TODO Si se quiere mantener la selección actual de combustible se descomenta.
          //if ($selectComb.html() !== response.combustibles) {
            $selectComb.html(response.combustibles);
          //}
        },
        error: function (response) {
          $('body').removeClass('loading');
        }
      });
    };



    /**
     * Actualiza la lista de combustibles de la marca y modelo.
     * @param tipoVehiculo
     * @param {string} tipoVehiculo - Identificador tipo de vehículo.
     * @param {string} marca - Identificador tipo de marca.
     * @param {string} modelo - Identificador modelo.
     */
    var cargaCombustibles = function(tipoVehiculo, marca, modelo) {

      var parametros = {
        action: 'options_ajax',
        t: tipoVehiculo,
        ma: marca,
        mo: modelo
      };

      $('body').addClass('loading');
      $.ajax({
        url: form_busqueda_ajax.url,
        dataType: "json",
        type: 'get',
        data: parametros,
        success: function (response) {
          $('body').removeClass('loading');
          $('select.combustible[t-vehiculo="' + tipoVehiculo + '"]').html(response.combustibles);
        },
        error: function () {
          $('body').removeClass('loading');
        }
      });
    };


    var spinnerButton =
      '<button class="btn btn-sm btn-info spinnner info">' +
        '<span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span>' +
        'Buscando...' +
      '</button>';


    var errorMsg =
      '<div class="alert alert-danger info">' +
        '<strong>Error!</strong> No se ha podido realizar la búsqueda, inténtelo de nuevo' +
      '</div>';



    /**
     * Devuelve los vehículos a según parámetros.
     * @param {json} query - Parámetros.
     */
    var buscarVehiculos = function (query, callback) {
      var $spinnerButton = $(spinnerButton);
      var $verResultado = $('.ver-resultados');
      $spinnerButton.appendTo($containerVehiculos);
      $('body').addClass('loading');
      $.ajax({
        url: form_busqueda_ajax.url,
        dataType: "json",
        type: 'get',
        data: query,
        success: function (response) {
          // HTML resultado.
          $containerVehiculos.html(response.resultado);
          // Actualizar el boton ver xx resultados
          $verResultado.text(response.total);
          // Se guarda los parámetros de búsqueda.
          $containerVehiculos.data('query', query);
          // Paginación actual.
          actualPage = parseInt($containerVehiculos.find('.page-item.active').text().trim());
          if (typeof callback === 'function') {
            callback();
          }
          $('body').removeClass('loading');
          $(document).trigger('busqueda-end');
        },
        error: function () {
          $spinnerButton.remove();
          // Mensaje de error.
          var $msg = $(errorMsg);
          $msg.appendTo($containerVehiculos);
          setTimeout(function () {
            $msg.hide().remove();
          }, 3000);
          $('body').removeClass('loading');
        }
      });
    };





    var isMobile = function () {
      var result = (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
        || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4)));
      if (!result) {
        result =  (('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch);
      }
      return result;
    };


    var initMobile = function () {
      var $allFormsBusquedaMobile = $('.form-busqueda-mobile');

      var hideFormBusquedaMobile = function () {
        $allFormsBusquedaMobile.hide();
        $('html').removeClass('form-busqueda-mobile-show');
      };


      if ($allFormsBusquedaMobile.length) {
        $allFormsBusquedaMobile.appendTo(document.body);

        $('.btn-show-form-busqueda-mobile').click(function(){
          var tipoVehiculo =  $(this).attr('t-vehiculo');
          $allFormsBusquedaMobile.hide();
          $allFormsBusquedaMobile.each(function () {
            var $fom = $(this);
            if (tipoVehiculo === $fom.attr('t-vehiculo')) {
              $fom.show();
              $('html').addClass('form-busqueda-mobile-show');
            }
          });
        });


        $('.btn-hide-form-busqueda-mobile').click(function(){
          hideFormBusquedaMobile();
        });


        $('.btn-buscador-mobile').click(function(){
          var tipoVehiculo = $(this).attr('t-vehiculo');
          var path_info = $(this).attr('path_info');
          var shownoimg = $(this).attr('shownoimg');
          var query = getBusquedaParams(tipoVehiculo, path_info, shownoimg);
          buscarVehiculos(query, function () {
            hideFormBusquedaMobile();
          });
        });

        $(document).on('click', '.search-mobile', function () {
          hideFormBusquedaMobile();
        });
        /* Oculta la ventana de filtros cada vez que seleccionas una opcion
        $(document).on('busqueda-end', function () {
          hideFormBusquedaMobile();
        });
        -------*/
      }

    };

    initMobile();

  });

  /**
   * Devuelve los parámetros seleccionados por el usuario.
   * @returns {json}
   */
  var getBusquedaParams = function (tipoVehiculo, path_info, shownoimg) {
    var filtro = {
      action: 'busqueda_ajax',
      t: tipoVehiculo,
      path_info: path_info,
      shownoimg: shownoimg,
      marca_shortcode: eval('marca_shortcode'),
      numcolsclass: eval('numcolsclass_' + tipoVehiculo),
      showpagination: eval('showpagination')
    };

    // Campos Combos
    var $selects = $('select[t-vehiculo="' + tipoVehiculo + '"][name!="order"]');
    $selects.each(function () {
      var $select = $(this);
      var val = $select.val();
      var $ismin = $(this).attr('name').split('min')[1];
      var $ismax = $(this).attr('name').split('max')[1];
      if (val) {
        if ($ismin){
          filtro['max'+$ismin] = $('option:last', this).val();
        }else if($ismax && typeof filtro['min'+$ismax] === 'undefined'){
          filtro['min'+$ismax] = $('option:eq(1)', this).val();
        }
        filtro[$select.attr('name')] = val;
      }
    });

    // Campos Slider
    if ( !$('select.mobile') || !window.matchMedia('(pointer:coarse)').matches) {
      var $sliders = $('.slider[t-vehiculo="' + tipoVehiculo + '"]');
      $sliders.each(function () {
        var $slider = $(this);
        var values = $slider[0].noUiSlider.get();
        filtro['min' + $slider.attr('name')] = values[0];
        filtro['max' + $slider.attr('name')] = values[1];
      });
    }

    // Campos Radios
    var $radios = $('.radio[t-vehiculo="' + tipoVehiculo + '"].selected');
    $radios.each(function () {
      var $radio = $(this);
      filtro[$radio.attr('name')] = $radio.attr('data-value');
    });

    var $orderOption = $('select[t-vehiculo="' + tipoVehiculo + '"][name="order"]');
    $orderOption.each(function () {
      var value = $(this).val();
      if (value) {
        var type = $orderOption.find('option[value="' + value +'"]').attr('type');
        if (type === 'orderbydesc') {
          value = value.split('-desc')[0];
        }
        filtro[type] = value;
      }else if (eval('orderby')){
        filtro['orderby'] = eval('orderby');
      }
    });


    return filtro;
  };



  var puntosNumero = function (value) {
    var retorno = '';
    value = value.toString().split('').reverse().join('');
    var i = value.length;
    while (i > 0) {
      retorno += ((i % 3 === 0 && i !== value.length) ? '.' : '') + value.substring(i--, i);
    }
    return retorno;
  };




})(jQuery);