<?php

require_once __DIR__ . '/../data/DataTipologias.php';


// Plantilla Tipología.

global $HTML_TEMPLATE_TIPOLOGIA;
$HTML_TEMPLATE_TIPOLOGIA = '<li name="tp" t-vehiculo="{$tipovehiculo}" class="radio tipologia girsanet-icons-{$class}" data-value="{$value}">{$value}</li>';


/**
 * Class HTMLTipologias
 */

class HTMLTipologias {


	/**
	 * Muestra la tipologías de un tipo de vehículo.
	 * @param object $mysqli Conexión a BBDD.
	 * @param string $tipovehiculo Indentificador tipo vehículo.
	 */
	public function printBy( $mysqli, $params ) {
		global $HTML_TEMPLATE_TIPOLOGIA;

		$dataTipologias = new DataTipologias();
		$tipologias = $dataTipologias->getBy($mysqli, $params['t']);

		if (!empty($tipologias)) {
			// Contenedor de tipologías.
			printf('<ul class="tipologias" t-vehiculo="' . $params['t'] . '" no-filter>');

			// Lista de tipologías
			while ($row = mysqli_fetch_assoc($tipologias)) {
				$tipologia = $row['value'];
				if (!empty($tipologia)) { // Elimina el valor vacío.
					$tipologiaClass = strtolower($tipologia);
                    $tipologiaClass = replaceAcentoGuion($tipologiaClass);
					$tipologiaClass = preg_replace( '/_+/', '-', $tipologiaClass );

					$options = array (
						'class' => $tipologiaClass,
						'value' => $tipologia,
						'tipovehiculo' => $params['t']
					);
					printTemplate($HTML_TEMPLATE_TIPOLOGIA, $options);
				}
			}
			// Fin contenedor.
            printf( '<input type="hidden" name="tp" value="" t-vehiculo="' . $params['t'] . '">');
			printf('</ul>');
		}
	}

}