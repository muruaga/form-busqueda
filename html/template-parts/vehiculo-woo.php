<?php
/**
 * Created by IntelliJ IDEA.
 * User: Luis
 * Date: 09/06/2020
 * Time: 15:00
 */


$id_vehiculos = $_POST['id'];;
$dataVehiculo = new DataVehiculo($id_vehiculos);
$vehiculo = $dataVehiculo->get();
$vehiculoList = parseDataVehiculo($vehiculo, array());

?>
<div class="contenedor-reservar">
  <div class="titulo-reservar caja-hover col-sm-6 col-md-10 col-lg-8 col-xl-6 text-center">
    <div class="box-car box-reservar">
      <input type="image" class="foto-reservar" src="<?php echo $vehiculo["foto"] ?>" name="id"
             value="<?php echo $vehiculo["id_vehiculos"] ?>">
      <p>
        <span class="t1"><strong><?php echo $vehiculo["maknatcode"] ?></strong></span>
        <span class="t2"><strong> <?php echo $vehiculo["mlocode"] ?></strong></span>
        <span class="t3"> <?php echo $vehiculo["modnatcode"] ?></span>
        <span> <?php echo $vehiculo["cv"] ?> Cv</span>
      </p>

      <?php if ($vehiculo["oferta"] == 0) { ?>
        <p><span class="precio-reservar"><strong><?php echo $vehiculo["precio"] ?> €</strong></span></p>
      <?php } else  { ?>
        <span class="tachado-reservar"><?php echo $vehiculo["precio"] ?> €</span>
        <p><span class="precio-reservar"><strong><?php echo $vehiculo["oferta"] ?> €</strong></span></p>
      <?php } ?>
    </div>
  </div>
</div>

<?php
unset($_SESSION['mar-coche']);
unset($_SESSION['mod-coche']);
unset($_SESSION['ver-coche']);
unset($_SESSION['url-coche']);

$_SESSION['mar-coche'] = $vehiculo["maknatcode"];
$_SESSION['mod-coche'] = $vehiculo["mlocode"];
$_SESSION['ver-coche'] = $vehiculo["modnatcode"];
$_SESSION['url-coche'] = $vehiculoList["ficha"];