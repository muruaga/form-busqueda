<div class="form-group col-md-12">
  <button class="btn btn-show-form-busqueda-mobile" t-vehiculo="<?php echo $tipovehiculo ?>">
    <span><i class="fas fa-filter"></i> Filtrar</span>
  </button>
</div>


<div class="form-busqueda-mobile container-fluid hidden" t-vehiculo="<?php echo $tipovehiculo ?>">

	<div class="form-container container">
    <div class="row form-busqueda-header">
      <!--boton cruz para ocultar -->
      <div class="form-group col-md-12">
        <button class="btn btn-hide-form-busqueda-mobile" t-vehiculo="<?php echo $tipovehiculo ?>">X</button>
      </div>
    </div>
    <form id="formSearchMobile" method="post" action="<?php echo $action ?>">
      <div class="row form-container-inside">

        <div class="form-group col-sm-12 bottom-30 form-busqueda-part ma">
          <label for="ma-mobile" class="text-center">¿Qué marcas buscas?</label>
          <select id="ma-mobile" class="form-control marca mobile" name="ma" t-vehiculo="<?php echo $tipovehiculo ?>" no-filter>
            <?php $formOptions->printAllMarcas($mysqli, null); ?>
          </select>
        </div>

        <div class="form-group col-sm-12 bottom-30 form-busqueda-part mo">
          <label for="mo-mobile" class="text-center">¿Buscas un modelo específico?</label>
          <select id="mo-mobile" class="form-control modelo mobile" name="mo" t-vehiculo="<?php echo $tipovehiculo ?>" no-filter>
            <option disabled="disabled" selected="selected">Modelo</option>
          </select>
        </div>

        <div class="form-group col-sm-12 bottom-30 form-busqueda-part co">
          <label for="co-mobile" class="text-center">¿Qué combustible deseas?</label>
          <select id="co-mobile" class="form-control combustible mobile" name="co" t-vehiculo="<?php echo $tipovehiculo ?>" no-filter>
            <?php $formOptions->printAllCombustibles($mysqli); ?>
          </select>
        </div>

        <div class="form-group col-sm-12 bottom-30 form-busqueda-part">
          <label class="text-center">Tu presupuesto</label>
        </div>

        <div class="form-group col-sm-12 bottom-30 form-busqueda-part">
          <select id="minprecio-mobile" class="form-control precio mobile" name="minprecio" t-vehiculo="<?php echo $tipovehiculo ?>" no-filter>
            <?php $formOptions->printAllPrecios($mysqli, 2000, null, true); ?>
          </select>
        </div>

        <div class="form-group col-sm-12 bottom-30 form-busqueda-part">
          <select id="maxprecio-mobile" class="form-control precio mobile" name="maxprecio" t-vehiculo="<?php echo $tipovehiculo ?>" no-filter>
            <?php $formOptions->printAllPrecios($mysqli, 2000, null, false); ?>
          </select>
        </div>

        <!--
        <div class="form-group col-sm-12 bottom-30 form-busqueda-part">
          <select id="minkm-mobile" class="form-control km mobile" name="minkm" t-vehiculo="<?php echo $tipovehiculo ?>" no-filter>
            <?php $formOptions->printAllKms($mysqli, 10000,  $minkm, true); ?>
          </select>
        </div>

        <div class="form-group col-sm-12 bottom-30 form-busqueda-part">
          <select id="maxkm-mobile" class="form-control km mobile" name="maxkm" t-vehiculo="<?php echo $tipovehiculo ?>" no-filter>
            <?php $formOptions->printAllKms($mysqli, 10000, $maxkm, false); ?>
          </select>
        </div>

        <div class="form-group col-sm-12 bottom-30 form-busqueda-part">
          <select id="minanno-mobile" class="form-control anno mobile" name="minanno" t-vehiculo="<?php echo $tipovehiculo ?>" no-filter>
            <?php $formOptions->printAllAnnos($mysqli, 1, $minanno, true); ?>
          </select>
        </div>

        <div class="form-group col-sm-12 bottom-30 form-busqueda-part">
          <select id="maxanno-mobile" class="form-control año mobile" name="maxanno" t-vehiculo="<?php echo $tipovehiculo ?>" no-filter>
            <?php $formOptions->printAllAnnos($mysqli, 1, $maxanno, false); ?>
          </select>
        </div>


        <div class="form-group col-md-12">
          <label for="order-mobile"  class="text-center">Ordenar por</label>
          <select id="order-mobile" class="form-control order mobile" name="order" t-vehiculo="<?php echo $tipovehiculo ?>" no-filter>
            <option selected="selected" disabled="disabled" type="">ninguno</option>
            <option value="maknatcode" type="orderby">marca</option>
            <option value="mlocode" type="orderby">modelo</option>
            <option value="pvpParticulares" type="orderby">precio ascendente</option>
            <option value="pvpParticulares-desc" type="orderbydesc">precio descendente</option>
          </select>
          <input type="hidden" name="orderby" t-vehiculo="<?php echo $tipovehiculo ?>">
          <input type="hidden" name="orderbydesc" t-vehiculo="<?php echo $tipovehiculo ?>">
        </div>
        -->
      </div>

      <div class="row form-busqueda-footer">
        <div class="form-group col-sm-6 text-center">
          <button type="submit" class="btn btn-buscador-mobile" t-vehiculo="<?php echo $tipovehiculo ?>">Buscar</button>
        </div>
        <div class="form-group col-sm-6 text-center">
          <button type="reset" class="btn clean" t-vehiculo="<?php echo $tipovehiculo ?>">Limpiar</button>
        </div>
      </div>
    </form>

	</div>


</div>