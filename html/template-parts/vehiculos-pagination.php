<?php
// calculamos la primera y última página a mostrar
if (($pag - 3) > 1 && $totalPages > $pag){
  if(($pag - 3) > ($totalPages - 6)){$primera = $totalPages - 6; }
  else { $primera = $pag - 3; }
}else{
  $primera = 1;
}
if (($pag + 3) < $totalPages && $totalPages > 7){
  if(($pag + 3) < 8){$ultima = 7;}
  else { $ultima = $pag + 3; }
}else{
  $ultima = $totalPages;
}
?>
<nav class="content-pagination">
  <ul class="pagination justify-content-center">
    <!-- Botón anterior -->
    <?php if ( $pag > 1 ){ ?>
      <li class="page-item prev" pag="<?php echo ( $start - VEHICULOS_POR_PAGINA )?>"><a class="page-link" href="#"> &laquo; </a></li>
    <?php } ?>
    <!-- Puntos suspensivos -->
    <?php if ( $primera > 1 ){ ?>
      <li class="page-item disabled"><a class="page-link" href="#"> ... </a></li>
    <?php } ?>
    <!-- Botones de paginación -->
    <?php
    $i = $primera;
    for ( ; $i <= $ultima; $i++ ) { $active = ( $i == $pag ) ? 'active' : ''; ?>
      <li class="page-item <?php echo $active ?>" pag="<?php echo $i ?>"><a class="page-link" href="#"><?php echo $i ?></a></li>
    <?php } ?>
    <!-- Puntos suspensivos -->
    <?php if ( $ultima < $totalPages){ ?>
      <li class="page-item disabled"><a class="page-link" href="#"> ... </a></li>
    <?php } ?>
    <!-- Botón siguiente -->
    <?php if ( $pag != $totalPages ) { ?>
      <li class="page-item next" pag="<?php echo ( $start + VEHICULOS_POR_PAGINA )?>"><a class="page-link" href="#"> &raquo; </a></li>
    <?php } ?>
  </ul>
</nav>