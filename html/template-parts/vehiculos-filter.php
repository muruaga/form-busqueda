<div class="form-busqueda container-fluid" t-vehiculo="<?php echo $tipovehiculo ?>">
  <div class="row">

    <div class="form-group col-sm-4 bottom-30 form-busqueda-part">
      <p class="text-center">¿Qué marcas buscas?</p>
      <select class="form-control marca" name="ma" t-vehiculo="<?php echo $tipovehiculo ?>">
        <?php
          $formOptions->printAllMarcas($mysqli, $ma);
        ?>
      </select>
    </div>

    <div class="form-group col-sm-4 bottom-30 form-busqueda-part">
      <p class="text-center">¿Buscas un modelo específico?</p>
      <select class="form-control modelo" name="mo" t-vehiculo="<?php echo $tipovehiculo ?>">
	      <?php
	        if (!empty($ma)) {
		        $formOptions->printModelos($mysqli, $ma, $mo);
          }
          else {
	          printf('<option disabled="disabled" selected="selected">Modelo</option>');
          }
        ?>
      </select>
    </div>

    <div class="form-group col-sm-4 bottom-30 form-busqueda-part">
      <p class="text-center">¿Qué combustible deseas?</p>
      <select class="form-control combustible" name="co" t-vehiculo="<?php echo $tipovehiculo ?>">
	      <?php
          if (!empty($ma)) {
            $formOptions->printCombustibles($mysqli, $ma, $mo, $co);
          }
          else {
            $formOptions->printAllCombustibles($mysqli, $co);
          }
        ?>
      </select>
    </div>

  </div>

	<?php
	if ($showsliders=='true') {
	?>

  <div class="form-group row selectores">

    <div class="form-group col-md-4 form-busqueda-part">
      <div class="form-group buscadores-rango">
        <div class="slider-title text-center">Tu presupuesto</div>
        <div class="form-control slider" name="precio" unit="€" min="<?php echo $minprecio ?>" max="<?php echo $maxprecio ?>" values="<?php echo $preciovalues ?>" t-vehiculo="<?php echo $tipovehiculo ?>"></div>
        <span class="slider-value min float-left" name="precio" t-vehiculo="<?php echo $tipovehiculo ?>"></span>
        <span class="slider-value max float-right" name="precio" t-vehiculo="<?php echo $tipovehiculo ?>"></span>
        <input type="hidden" name="preciovalues" value="" t-vehiculo="<?php echo $tipovehiculo ?>">
      </div>
    </div>

    <div class="form-group col-md-4 form-busqueda-part">
      <div class="form-group buscadores-rango">
        <div class="slider-title text-center">Kilómetros</div>
        <div class="form-control slider" name="km" unit="kms" min="<?php echo $minkm ?>" max="<?php echo $maxkm ?>" values="<?php echo $kmvalues ?>" t-vehiculo="<?php echo $tipovehiculo ?>"></div>
        <span class="slider-value min float-left" name="km" t-vehiculo="<?php echo $tipovehiculo ?>"></span>
        <span class="slider-value max float-right" name="km" t-vehiculo="<?php echo $tipovehiculo ?>"></span>
        <input type="hidden" name="kmvalues" value="" t-vehiculo="<?php echo $tipovehiculo ?>">
      </div>
    </div>

    <div class="form-group col-md-4 form-busqueda-part">
      <div class="form-group buscadores-rango">
        <div class="slider-title text-center">Antigüedad</div>
        <div class="form-control slider" name="anno" unit="año(s)" min="<?php echo $minanno ?>" max="<?php echo $maxanno ?>" values="<?php echo $annovalues ?>" t-vehiculo="<?php echo $tipovehiculo ?>"></div>
        <span class="slider-value min float-left" name="anno" t-vehiculo="<?php echo $tipovehiculo ?>"></span>
        <span class="slider-value max float-right" name="anno" t-vehiculo="<?php echo $tipovehiculo ?>"></span>
        <input type="hidden" name="annovalues" value="" t-vehiculo="<?php echo $tipovehiculo ?>">
      </div>
    </div>
  </div>
	<?php } ?>

	<?php
	if ($showtipologia=='true') {
	?>
  <div class="form-group col-sm-12 form-busqueda-part">
	  <?php $tipologias->printBy( $mysqli, $tipovehiculo ); ?>
  </div>
  <?php } ?>

  <div class="form-group row">
    <div class="form-group col-md-6">
      <select class="form-control order" name="order" t-vehiculo="<?php echo $tipovehiculo ?>">
        <option selected="selected" disabled="disabled" type="">Ordenar por</option>
        <option value="maknatcode" type="orderby">marca</option>
        <option value="mlocode" type="orderby">modelo</option>
        <option value="pvpParticulares" type="orderby">precio ascendente</option>
        <option value="pvpParticulares-desc" type="orderbydesc">precio descendente</option>
      </select>
    </div>
    <div class="form-group col-md-6">
      <button class="reset qbutton btn-buscador enlarge" t-vehiculo="<?php echo $tipovehiculo ?>">Ver todo</button>
    </div>
  </div>
</div>