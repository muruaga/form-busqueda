<?php
/**
 * Created by IntelliJ IDEA.
 * User: Huawei
 * Date: 04/06/2018
 * Time: 21:42
 */

// HTML not result.
global $HTML_TEMPLATE_NOT_RESULT;
$HTML_TEMPLATE_NOT_RESULT =
	'<div class="row vehiculos">
		<div class="col-md-12">
			<h4>{$msg}</h4>
		</div>
	</div>';


// Consulta los vehículos.
require_once __DIR__ . '/../data/DataVehiculos.php';


class HTMLVehiculos {


	/**
	 * Muestra los vehículos según
	 *
	 * @param $mysqli
	 * @param $params
	 */
	public function printBy( $mysqli, $params ) {
		global $HTML_TEMPLATE_NOT_RESULT;
		global $FORM_LOCALES;

		$dataVehiculos = new DataVehiculos( $mysqli );
		$tipo_vehiculos = $params['t'];
		$data          = $dataVehiculos->getBy( $params );
		$numcolsclass  = $params['numcolsclass'];

		// Sin resultados en la búsqueda.
		if ( $data['total'] == 0 ) {
			$options = array(
				'msg' => $FORM_LOCALES['not-result']
			);
			printTemplate( $HTML_TEMPLATE_NOT_RESULT, $options );
		} else {
			$this->printResult( $tipo_vehiculos, $data['result'], $numcolsclass, $params );
            if ( ($params['showpagination'] === 'true' || $params['showpagination'] === true) && $params['start'] != 'ALL') {
              $this->printPaginacion($params['pag'], $data['total']);
            }
		}
	}


	/**
	 * @param $result
	 */
	private function printResult( $tipovehiculo, $result, $numcolsclass, $params ) {

		$data = array();
		while ( $row = mysqli_fetch_assoc( $result ) ) {
			$data[] = $row;
		}
		if (TEMPLATE_VEHICULOS_LIST) {
			include get_stylesheet_directory() . '/' . PLUGIN_NAME . '/template-parts/vehiculos-list.php';
		}
		else {
			include 'template-parts/vehiculos-list.php';
		}

	}


	/**
	 * @param $pag
	 * @param $total
	 *
	 */
	private function printPaginacion( $pag, $total ) {

		if ( empty( $pag ) ) {
			$pag = 1;
		}

		// Botones de paginación.
		$totalPages = ceil( $total / VEHICULOS_POR_PAGINA );
		if ( $totalPages > 1 ) {
			$start = 1;
			if (TEMPLATE_VEHICULOS_PAGINATION) {
				include get_stylesheet_directory() . '/' . PLUGIN_NAME . '/template-parts/vehiculos-pagination.php';
			}
			else {
				include 'template-parts/vehiculos-pagination.php';
			}

		}
	}
}